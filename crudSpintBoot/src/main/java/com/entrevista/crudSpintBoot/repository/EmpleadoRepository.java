package com.entrevista.crudSpintBoot.repository;

import org.springframework.data.repository.CrudRepository;

import com.entrevista.crudSpintBoot.entity.Empleado;

public interface EmpleadoRepository extends CrudRepository<Empleado, Long>{

}
