package com.entrevista.apirestSpringBoot.service;

import java.util.ArrayList;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.entrevista.apirestSpringBoot.models.Empleado;
import com.entrevista.apirestSpringBoot.repository.EmpleadoRepository;

@Service
public class EmpleadoService {

	  @Autowired
	    EmpleadoRepository empleadoRepository;
	    
	    public ArrayList<Empleado> obtenerEmpleado(){
	        return (ArrayList<Empleado>) empleadoRepository.findAll();
	    }

	    public Empleado guardarEmpleado(Empleado empleado){
	        return empleadoRepository.save(empleado);
	    }

	    public Optional<Empleado> obtenerPorId(Long id){
	        return empleadoRepository.findById(id);
	    }

	    public boolean eliminarEmpleado(Long id) {
	        try{
	            empleadoRepository.deleteById(id);
	            return true;
	        }catch(Exception err){
	            return false;
	        }
	    }
}
