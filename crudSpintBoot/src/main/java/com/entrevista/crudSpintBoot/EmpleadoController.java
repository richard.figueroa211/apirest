package com.entrevista.crudSpintBoot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.entrevista.crudSpintBoot.entity.Empleado;
import com.entrevista.crudSpintBoot.repository.EmpleadoRepository;

@Controller
public class EmpleadoController {

	@Autowired
	EmpleadoRepository repository;
	
	@GetMapping
	public String index(Model model, Empleado empleado) {
		
		model.addAttribute("empleado", new Empleado());
		model.addAttribute("empleados", repository.findAll()); //SELECT * FROM EMPLEADOS
		
		return "index";
	}

	@PostMapping("/crearEmpleado")
	public String crearEmpleado(Model model, Empleado empleado) {
		repository.save(empleado); // INSERT INTO EMPLEADO
		model.addAttribute("empleado", new Empleado());
		model.addAttribute("empleados", repository.findAll());
		
		
		
		return "index";
	}
	
	@GetMapping("/editarEmpleado/{id}")
	public String editarEmpleado(Model model, @PathVariable(name="id") Long id) {
		Empleado empleadoparaEditar = repository.findById(id).get();
		model.addAttribute("empleado",empleadoparaEditar);
		model.addAttribute("empleados",repository.findAll());//SELECT * FROM USUARIOS;
		
		return "index";
	}

	@GetMapping("/eliminarEmpleado/{id}")
	public String eliminarUsuario(Model model, @PathVariable(name="id") Long id) {
		Empleado empleadoEliminar = repository.findById(id).get();
		repository.delete(empleadoEliminar);
		model.addAttribute("empleado",new Empleado());
		model.addAttribute("empleados",repository.findAll());//SELECT * FROM USUARIOS;
		return "index";
	}
	
}
