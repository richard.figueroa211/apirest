package com.entrevista.crudSpintBoot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.entrevista.crudSpintBoot.entity.Empleado;
import com.entrevista.crudSpintBoot.repository.EmpleadoRepository;

@SpringBootApplication
public class CrudSpintBootApplication {

	public static void main(String[] args) {
		
		ConfigurableApplicationContext configurableApplicationContext =
				SpringApplication.run(CrudSpintBootApplication.class, args);
		EmpleadoRepository empleadoRepository=
				configurableApplicationContext.getBean(EmpleadoRepository.class);
		
		Empleado myEmpleado = new Empleado(1,"ENMANUEL","26.586.409-0","ADMINISTRADOR/JEFE DE PROYECTOS");
		Empleado myEmpleado2 = new Empleado(2,"MANUEL","17.838.166-0","CLIENTE");
		Empleado myEmpleado3 = new Empleado(3,"GUILLERMO LOPEZ","23.976.387-1","ADMINISTRADOR/CLIENTE");
		empleadoRepository.save(myEmpleado);
		empleadoRepository.save(myEmpleado2);
		empleadoRepository.save(myEmpleado3);
	}

}
